#!/bin/bash
# This script assumes that you have the deployer ssh key in your home directory
# The ./public directory is deployed into the $DEPLOY_TO directory on the remote servers

RSYNC=/usr/bin/rsync
SOURCE_FOLDER="./_site/*"
PRODUCTION_HOSTNAMES=("vps1.labnux.com")
DEPLOY_TO=/var/www/blog.fuzzybee.me/
DEPLOYER_SSH_KEY=$HOME/.ssh/id_rsa
LOGFILE=deploy.log

echo -n "Warning: This will deploy directly to the production servers. Are you sure? (y/N): "
read -t 10 confirm

if [ "$confirm" != "y" ]; then
echo -e "\nAborting...\n"
    exit 0
else
echo -e "\nDeploying...\n"
fi

echo "" > $LOGFILE

echo -n "Building the jekyll application..."
echo "Building the jekyll application..." >> $LOGFILE
bundle exec jekyll build --config=_config.yml >> $LOGFILE
if [ $? -eq 0 ]; then
echo "done."
else
echo -e "\nAn error occurred when building the application."
    exit 1
fi

echo "Rsyncing to production servers"
for HOSTNAME in "${PRODUCTION_HOSTNAMES[@]}"
do
    echo -n "Syncing to $HOSTNAME..."
    echo -e "\n\nSyncing to $HOSTNAME..." >> $LOGFILE
    $RSYNC -avz --delete --rsh="ssh -i $DEPLOYER_SSH_KEY" "$SOURCE_FOLDER" root@"$HOSTNAME":$DEPLOY_TO >> $LOGFILE
    ssh root@"$HOSTNAME" '/root/fixperms.sh'
    if [ $? -eq 0 ]; then
        echo "done."
    else
        echo -e "\nAn error occured when deploying to $HOSTNAME. Check deploy.log for details"
    fi
done